-Push an existing folder
 cd existing_folder
git init
git remote add origin git@gitlab.com:youngwoo5880/test-app-api-proxy.git
git add .
git commit -m "Initial commit"
git push -u origin master



#test APP API Proxy

NGINX proxy app for our test app api

## Usage
### Environment Variable  
* 'LISTEN_PORT - Port to listen on (default: '8000')
* 'APP_HOST' - Hostname of the app to forward requests to (default: 'app') 
* 'APP_PORT' - Port of the app to forward requests to (default: '9000')

